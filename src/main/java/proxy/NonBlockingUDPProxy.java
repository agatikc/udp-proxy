package proxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;

public class NonBlockingUDPProxy extends AbstractProxy {
    private static final int DEFAULT_COUNT_THREADS = 10;
    private final Selector selector;
    private final List<DatagramChannel> proxyChannels;
    private final BlockingQueue<ByteBuffer> byteBufferPool;

    private final Map<SocketAddress, SocketAddress> portToServer;
    private final Map<SocketAddress, SocketAddress> serverToClient;

    private final BlockingQueue<FromClientToServer> fromClientToServer;
    private final BlockingQueue<FromServerToClient> fromServerToClient;

    public static void main(final String[] args) {
        if (args == null || args.length != 1 || args[0] == null) {
            Util.log("Wrong args");
            return;
        }

        final Path config;
        try {
            config = Path.of(args[0]);
        } catch (final InvalidPathException e) {
            Util.log("Wrong path", e);
            return;
        }


        final List<Redirect> redirects = new ArrayList<>();
        try (final Scanner scanner = new Scanner(config)) {
            try {
                final int localPort = scanner.nextInt();
                final String remoteHost = scanner.next();
                final int remotePort = scanner.nextInt();
                redirects.add(new Redirect(localPort, remoteHost, remotePort));
            } catch (final NoSuchElementException e) {
                Util.log("incorrect file contents", e);
                return;
            }
        } catch (final IOException e) {
            Util.log("Fail open input file", e);
            return;
        }

        try (final NonBlockingUDPProxy ignored = new NonBlockingUDPProxy(redirects, DEFAULT_COUNT_THREADS)) {
            try {
                System.in.read();
            } catch (final IOException e) {
                Util.log("Fail read System.in for close proxy", e);
            }
        } catch (final IOException e) {
            Util.log("Fail start proxy", e);
        }
    }

    public NonBlockingUDPProxy(final List<Redirect> redirects, final int countThreads) throws IOException {
        if (countThreads < 1) {
            throw new IllegalArgumentException("count threads less 1");
        }
        try {
            selector = Selector.open();
            proxyChannels = new ArrayList<>();
            portToServer = new ConcurrentHashMap<>();
            serverToClient = new ConcurrentHashMap<>();
            for (final Redirect r : redirects) {
                final DatagramChannel proxyChannel = DatagramChannel.open();
                proxyChannel.bind(new InetSocketAddress(r.getLocalPort()));
                if (portToServer.put(proxyChannel.getLocalAddress(), r.getRemoteAddress()) != null) {
                    throw new IllegalArgumentException("Duplicate local ports values in redirects");
                }
                proxyChannel.configureBlocking(false);
                proxyChannel.register(selector, SelectionKey.OP_READ);
                proxyChannels.add(proxyChannel);
            }

            byteBufferPool = Util.createByteBufferPool(countThreads * TASKS_ON_THREAD,
                    proxyChannels.get(0).getOption(StandardSocketOptions.SO_RCVBUF));

            fromClientToServer = new ArrayBlockingQueue<>(countThreads * TASKS_ON_THREAD);
            fromServerToClient = new ArrayBlockingQueue<>(countThreads * TASKS_ON_THREAD);
            startThreads(countThreads);
        } catch (final IOException | IllegalArgumentException e) {
            close();
            throw e;
        }
    }

    @Override
    protected void listener() {
        while (!Thread.currentThread().isInterrupted() && selector.isOpen()) {
            try {
                selector.select();
            } catch (final IOException e) {
                Util.log("Fail select", e);
                return;
            }

            for (final Iterator<SelectionKey> i = selector.selectedKeys().iterator(); i.hasNext(); ) {
                final SelectionKey key = i.next();
                try {
                    if (key.isReadable()) {
                        readKey(key);
                    }
                    if (key.isWritable()) {
                        writeKey(key);
                    }
                } finally {
                    i.remove();
                }
            }
        }
    }

    private void readKey(final SelectionKey key) {
        final ByteBuffer receiveBuffer = byteBufferPool.poll();
        if (receiveBuffer == null) {
            return;
        }
        receiveBuffer.clear();

        final DatagramChannel channel = (DatagramChannel) key.channel();

        final SocketAddress sourceAddress;
        try {
            sourceAddress = channel.receive(receiveBuffer);
        } catch (final IOException e) {
            Util.log("Fail receive", e);
            return;
        }

        try {
            workers.submit(() -> {
                receiveBuffer.flip();
                final SocketAddress clientAddress = serverToClient.get(sourceAddress);
                if (clientAddress != null) { // the response came from the server
                    try {
                        fromServerToClient.add(new FromServerToClient(clientAddress, receiveBuffer));
                    } catch (final IllegalStateException e) {
                        Util.log("Fail add answer from server to client", e);
                        return;
                    }
                } else { // the response came from the client
                    final SocketAddress localAddress;
                    try {
                        localAddress = channel.getLocalAddress();
                    } catch (final IOException e) {
                        Util.log("Fail get local address", e);
                        return;
                    }
                    serverToClient.putIfAbsent(localAddress, sourceAddress);
                    try {
                        fromClientToServer.add(new FromClientToServer(localAddress, receiveBuffer));
                    } catch (final IllegalStateException e) {
                        Util.log("Fail add answer", e);
                        return;
                    }
                }
                key.interestOpsOr(SelectionKey.OP_WRITE);
                selector.wakeup();
            });
        } catch (final RejectedExecutionException e) {
            Util.log("Fail add task on workers", e);
        }
    }

    private void writeKey(final SelectionKey key) {
        final DatagramChannel channel = (DatagramChannel) key.channel();
        final FromClientToServer packetFromClient = fromClientToServer.poll();
        if (packetFromClient != null) {
            try {
                channel.send(packetFromClient.buffer(), portToServer.get(packetFromClient.localPort()));
            } catch (final IOException e) {
                Util.log("Fail send from client to server", e);
            } finally {
                byteBufferPool.add(packetFromClient.buffer());
            }
            return;
        }

        final FromServerToClient packetFromServer = fromServerToClient.poll();
        if (packetFromServer == null) {
            key.interestOps(SelectionKey.OP_READ);
        } else {
            try {
                channel.send(packetFromServer.buffer(), packetFromServer.clientAddress());
            } catch (final IOException e) {
                Util.log("Fail send from server to client", e);
            } finally {
                byteBufferPool.add(packetFromServer.buffer());
            }
        }
    }


    @Override
    public void closeImpl() {
        Util.closeMaybeNull("selector", selector);
        for (int i = 0; i < proxyChannels.size(); i++) {
            Util.closeMaybeNull("proxy channel, number" + i, proxyChannels.get(i));
        }
    }

    private record FromClientToServer(SocketAddress localPort, ByteBuffer buffer) {
    }

    private record FromServerToClient(SocketAddress clientAddress, ByteBuffer buffer) {
    }

    private static final class Redirect {
        private final int localPort;
        private final InetSocketAddress remoteAddress;

        public Redirect(final int localPort, final String remoteHost, final int remotePort) {
            this.localPort = localPort;
            this.remoteAddress = new InetSocketAddress(remoteHost, remotePort);
        }

        public int getLocalPort() {
            return localPort;
        }

        public InetSocketAddress getRemoteAddress() {
            return remoteAddress;
        }
    }
}
