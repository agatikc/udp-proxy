package proxy;

import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Util {
    private static final long SHUTDOWN_SECOND = 20;

    public static void closeMaybeNull(final String name, final AutoCloseable o) {
        if (o != null) {
            try {
                o.close();
            } catch (final Exception e) {
                log("Fail close" + name, e);
            }
        }
    }

    public static void shutdownMaybeNull(final ExecutorService o) {
        if (o != null) {
            o.shutdown();
        }
    }

    public static void log(final String s, final Exception e) {
        System.err.println(s + " exception: " + e.getMessage());
    }

    public static void log(final String s) {
        System.err.println(s);
    }

    public static BlockingQueue<ByteBuffer> createByteBufferPool(final int countBuffers, final int sizeOfBuffer) {
        final BlockingQueue<ByteBuffer> pool = new ArrayBlockingQueue<>(countBuffers);
        for (int i = 0; i < countBuffers; i++) {
            pool.add(ByteBuffer.allocate(sizeOfBuffer));
        }
        return pool;
    }

    public static void shutdownAndAwaitTermination(final String poolName, final ExecutorService pool) {
        if (pool == null) {
            return;
        }

        try {
            if (!pool.awaitTermination(SHUTDOWN_SECOND, TimeUnit.SECONDS)) {
                pool.shutdownNow();
                if (!pool.awaitTermination(SHUTDOWN_SECOND, TimeUnit.SECONDS)) {
                    log(poolName + " not terminated");
                }
            }
        } catch (final InterruptedException e) {
            pool.shutdownNow();
            log(poolName + " not terminated");
            Thread.currentThread().interrupt();
        }
    }
}
