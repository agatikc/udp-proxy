package proxy;

import java.util.concurrent.*;

public abstract class AbstractProxy implements AutoCloseable {
    protected static final int MIN_POOL_TREADS_SIZE = 1;
    protected static final int KEEP_ALIVE_TIME = 2;
    protected static final TimeUnit KEEP_ALIVE_UNIT = TimeUnit.SECONDS;
    protected static final int TASKS_ON_THREAD = 500;
    protected static final RejectedExecutionHandler REJECTED_EXECUTION_HANDLER = new ThreadPoolExecutor.DiscardOldestPolicy();
    protected ExecutorService listener;
    protected ExecutorService workers;

    protected final void startThreads(final int maxPoolThreadsSize) {
        listener = Executors.newSingleThreadExecutor();
        workers = new ThreadPoolExecutor(
                MIN_POOL_TREADS_SIZE,
                maxPoolThreadsSize,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_UNIT,
                new ArrayBlockingQueue<>(maxPoolThreadsSize * TASKS_ON_THREAD),
                REJECTED_EXECUTION_HANDLER);
        listener.submit(this::listener);
    }


    protected abstract void listener();

    @Override
    public void close() {
        Util.shutdownMaybeNull(listener);
        Util.shutdownMaybeNull(workers);

        Util.shutdownAndAwaitTermination("listener", listener);
        Util.shutdownAndAwaitTermination("workers", workers);

        closeImpl();
    }

    protected abstract void closeImpl();
}
